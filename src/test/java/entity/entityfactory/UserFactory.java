package entity.entityfactory;

import entity.User;
import entity.entityenam.UserEntity;

/**
 * Фабрика объектов User, которая используется для создания и изменения пользовательских объектов
 */
public class UserFactory {
    public static User createUser() {
        return new User(UserEntity.NAME.getFieldName(), UserEntity.JOB.getFieldName());
    }
}