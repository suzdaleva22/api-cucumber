package entity;

public record UserDetails(Integer id, String email, String first_name, String last_name, String avatar) {
}