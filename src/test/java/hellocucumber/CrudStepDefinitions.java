package hellocucumber;

import entity.ListOfUsers;
import entity.User;
import entity.entityfactory.UserFactory;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import utils.ConfigurationReader;

import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Step definitions for CRUD operations on users in the REST API.
 */
public class CrudStepDefinitions {

    private Response response;

    /**
     * Sets the base URI for REST API requests
     * @param s the base URL to be used for the REST API
     */
    @Given("the base URI is get to {string}")
    public void theBaseURIIsIetToHttpsReqresInApiUsers(String s) {
        RestAssured.baseURI = ConfigurationReader.get("baseUrl");
    }

    /**
     * Sends a POST request to create a new user.
     * The user data is created using the {@link UserFactory#createUser()} method.
     */
    @When("I create a new user")
    public void iCreateANewUser() {
        response = (Response) given()
                .header("Content-Type", "application/json")
                .body(UserFactory.createUser())
                .log()
                .all()
                .when()
                .post( "/api/users");
        response.then().log().all();
    }

    /**
     * Asserts that the response status code matches the expected status code.
     * @param statusCode
     */
    @Then("status code is {int}")
    public void statusCodeIs(int statusCode) {
        assertEquals(statusCode, response.statusCode());
    }

    /**
     * Sends a GET request to retrieve the list of users.
     * The API endpoint queried is "/api/users?page=2".
     */
    @When("I request the list of users")
    public void iRequestTheListOfUsers() {
        response = given()
                .when()
                .get("/api/users?page=2");
        response.then().log().all();
    }

    /**
     * Asserts that the response contains a non-null list of users.
     * The list is deserialized into a {@link ListOfUsers} object.
     */
    @And("the response-list should contain a non-null")
    public void theResponseShouldContainANonNull() {
        ListOfUsers listOfUsers = response
                .then()
                .statusCode(SC_OK)
                .extract()
                .as(ListOfUsers.class);

        assertThat(listOfUsers).isNotNull();
        System.out.println(listOfUsers);
    }

    /**
     * Sends a PUT request to update a user's information.
     * The new user data is provided as parameters and constructed into a {@link User} object.
     * @param name the new name for the user
     * @param job  the new job title for the user
     */
    @When("I update the user's information to {string} with job {string}")
    public void iUpdateTheUserSInformationToWithJob(String name, String job) {
        User updatedUser = new User(name, job);

        response = given()
                .header("Content-Type", "application/json")
                .body(updatedUser)
                .log()
                .all()
                .when()
                .put("/api/users/2")
                .then()
                .log()
                .all()
                .extract()
                .response();
    }

    /**
     * Asserts that the response contains a non-null field "updatedAt".
     */
    @And("the response-updated should contain a non-null")
    public void theResponseUpdatedShouldContainANonNull() {
        assertThat(response.jsonPath().getString("updatedAt")).isNotNull();
    }

    /**
     * Sends a DELETE request to delete a user at the given endpoint.
     * The API endpoint is specified by the parameter.
     */
    @When("I send a DELETE request to {string}")
    public void iSendADELETERequestTo(String s) {

        response = given()
                .log()
                .all()
                .when()
                .delete("/api/users/2")
                .then()
                .extract()
                .response();
        response.then().log().all();
    }
}

