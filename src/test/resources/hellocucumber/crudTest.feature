Feature: Test CRUD for https://reqres.in

  Scenario: create new user
    Given the base URI is get to "https://reqres.in"
    When I create a new user
    Then status code is 201

  Scenario: Get list of users from page 2
    Given the base URI is get to "https://reqres.in"
    When I request the list of users
    Then status code is 200
    And the response-list should contain a non-null

  Scenario Outline: Update an existing user's information
    Given the base URI is get to "https://reqres.in"
    When I update the user's information to "<name>" with job "<job>"
    Then status code is 200
    And the response-updated should contain a non-null

    Examples:
      | name     | job           |
      | morpheus | zion resident |
      | neo      | the one       |
      | trinity  | operator      |

  Scenario: Delete an existing user
    Given the base URI is get to "https://reqres.in"
    When I send a DELETE request to "/api/users/2"
    Then status code is 204